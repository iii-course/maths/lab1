export enum Methods {
  FIBONACCI = 'Метод Фібоначчі',
  FASTEST_GRADIENT_DESCENT = 'Метод найшвидшого градієнтного спуску',
}

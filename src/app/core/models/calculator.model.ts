import {Observable} from "rxjs";
import {OneDimensionalResult, MultiDimensionalResult} from "./result-interfaces";

export interface ExtremumCalculator {
  iterations$: Observable<any[]>;
  getResult: (input: any) =>
  OneDimensionalResult |
  MultiDimensionalResult |
  Error;
}

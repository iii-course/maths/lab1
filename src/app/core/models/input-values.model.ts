import {Interval} from "./interval.model";
import {FunctionOfManyArguments, FunctionOfOneArgument} from "./functions-types";
import {FunctionData} from "./function-data";
import {NumericCoordinates} from "./vectors/coordinates.model";

export interface InputValues {}

export interface FibonacciInputValues extends InputValues {
  interval?: Interval,
  resultIntervalLength?: number,
  constant?: number
  func?: FunctionOfOneArgument | FunctionOfManyArguments
}

export interface FastestGradientDescentInputValues extends InputValues {
  x0: NumericCoordinates,
  e1: number,
  e2: number,
  M: number,
  funcData: FunctionData
}

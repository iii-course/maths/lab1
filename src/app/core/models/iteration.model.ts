import {Interval} from "./interval.model";
import {NumericCoordinates} from "./vectors/coordinates.model";

export interface Iteration {
  numberOfIteration: number
}

export interface FibonacciIteration extends Iteration {
  interval: Interval,
  y?: number,
  z?: number,
  fy?: number,
  fz?: number,
  x?: number
}

export interface FastestGradientDescentIteration extends Iteration {
  t: number,
  x: NumericCoordinates,
  xNext: NumericCoordinates,
  gradient: NumericCoordinates
}

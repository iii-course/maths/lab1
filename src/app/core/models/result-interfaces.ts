import {Interval} from "./interval.model";
import {NumericCoordinates} from "./vectors/coordinates.model";

export interface OneDimensionalResult {
  extremumPoint: number,
  interval: Interval,
  functionValue: number
}

export interface MultiDimensionalResult {
  extremumPoint: NumericCoordinates,
  functionValue: number
}

import {FunctionOfOneArgument, FunctionOfManyArguments} from "./functions-types";
import {Methods} from "./methods-enum";

export interface FunctionData {
  plainTextFormula: string,
  func: FunctionOfOneArgument | FunctionOfManyArguments,
  methodsAvailable: Methods[];
  argumentsNames: string[];
}

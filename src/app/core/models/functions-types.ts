import {NumericCoordinates} from "./vectors/coordinates.model";

export type FunctionOfOneArgument = (x: number) => number;
export type FunctionOfManyArguments = (...args: NumericCoordinates) => number;

import {FunctionData} from "../function-data";
import {NumericVector} from "./vector.model";
import {MathNode} from "mathjs";
import {NamedCoordinates, NumericCoordinates} from "./coordinates.model";
import * as math from "mathjs";

export class Gradient {
  public vector: MathNode[] = [];

  constructor(private funcData: FunctionData) {
    this.funcData.argumentsNames.forEach((argument: string) => {
      const partialDerivative = math.derivative(this.funcData.plainTextFormula, argument);
      this.vector.push(partialDerivative);
    });
  }

  public calculate(coordinates: NumericCoordinates): NumericVector {
    const mappedCoordinates = this.mapCoordinatesArrayIntoNamedCoordinates(coordinates);
    const calculatedGradient: number[] = [];
    this.vector.forEach(node => {
      calculatedGradient.push(
        node.evaluate(mappedCoordinates)
      );
    });
    return new NumericVector(calculatedGradient);
  }

  private mapCoordinatesArrayIntoNamedCoordinates(coordinates: NumericCoordinates): NamedCoordinates {
    const {argumentsNames} = this.funcData;
    if (coordinates.length !== argumentsNames.length) {
      throw new Error('Coordinates do not match provided arguments names');
    }
    const mapped: NamedCoordinates = {};
    argumentsNames.forEach((argumentName: string, i: number) => {
      mapped[argumentName] = coordinates[i];
    });
    return mapped;
  }
}

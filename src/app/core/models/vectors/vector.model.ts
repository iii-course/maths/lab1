import {NumericCoordinates} from "./coordinates.model";

export class NumericVector {
  coordinates: NumericCoordinates = [];

  constructor(value: number[]) {
    this.coordinates = value;
  }

  public get length(): number {
    return this.coordinates.length;
  }

  public get norm(): number {
    const sumSquare: number = this.coordinates.reduce(
      (prevVal, curVal) => prevVal + Math.pow(curVal, 2), 0
    );
    return Math.sqrt(sumSquare);
  }

  public plus(vectorToAdd: NumericVector): NumericVector {
    if (vectorToAdd.length !== this.length) {
      throw new Error('Cannot add vectors with different lengths');
    }
    const sum = [];
    for (let i = 0; i < this.length; i += 1) {
      sum[i] = this.coordinates[i] + vectorToAdd.coordinates[i];
    }
    return new NumericVector(sum);
  }

  public minus(vectorToSubtract: NumericVector): NumericVector {
    return vectorToSubtract ? this.plus(vectorToSubtract.multiplyBy(-1)) : this;
  }

  public multiplyBy(num: number): NumericVector {
    const result = [];
    for (let i = 0; i < this.length; i += 1) {
      result[i] = this.coordinates[i] * num;
    }
    return new NumericVector(result);
  }
}

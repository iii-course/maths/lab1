import { TestBed } from '@angular/core/testing';
import { SvenAlgorithmCalculatorService } from './sven-algorithm-calculator.service';
import {FunctionOfOneArgument} from "../../models/functions-types";

describe('SvenAlgorithmCalculatorServiceService', () => {
  let service: SvenAlgorithmCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SvenAlgorithmCalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return valid interval for (x - 5)^2', () => {
    const func: FunctionOfOneArgument = (x) => Math.pow((x - 5), 2);
    const x0 = 1;
    const step = 1;
    const interval = service.getInterval({func, x0, step});

    expect(interval).toBeDefined();
    expect(interval.begin).toBe(2);
    expect(interval.end).toBe(8);
  });
});

import { Injectable } from '@angular/core';
import {FunctionOfOneArgument} from "../../models/functions-types";
import {Interval} from "../../models/interval.model";
import {generateRandomInteger} from "../../utils/numbers";

interface Inputs {
  func: FunctionOfOneArgument,
  x0?: number,
  step?: number
}

@Injectable({
  providedIn: 'root'
})
export class SvenAlgorithmCalculatorService {
  private defaultX = 0;
  private defaultStep = 0.1;

  public getInterval({func, x0, step}: Inputs): Interval {
    const x = [x0 || this.defaultX];
    const t = step || this.defaultStep;

    let k = 0;
    let a!: number, b!: number;
    let f!: number, fLeft!: number, fRight!: number;

    do {
      f = func(x[0]);
      fLeft = func(x[0] - t);
      fRight = func(x[0] + t);
      if (fLeft < f && f > fRight) {
        x[0] = generateRandomInteger();
      }
    } while (fLeft < f && f > fRight);

    if (fLeft >= f && f <= fRight) {
      return new Interval({begin: x[0] - t, end: x[0] + t});
    } else {
      let delta!: number;
      if (fLeft >= f && f >= fRight) {
        delta = t;
        a = x[0];
      } else if (fLeft <= f && f <= fRight) {
        delta = -t;
        b = x[0];
      }

      x[1] = x[0] + delta;
      k += 1;

      let fNext!: number;
      do {
        x[k + 1] = x[k] + Math.pow(2, k) * delta;
        fNext = func(x[k + 1]);
        f = func(x[k]);
        if (fNext < f) {
          if (delta === t) {
            a = x[k];
          } else { // delta === -t
            b = x[k];
          }
          k += 1;
        }
      } while (fNext < f);

      if (delta === t) {
        b = x[k + 1];
      } else { // delta === -t
        a = x[k + 1];
      }
      return new Interval({begin: a, end: b});
    }
  }
}

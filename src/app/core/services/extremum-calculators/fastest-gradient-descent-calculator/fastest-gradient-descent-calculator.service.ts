import {Injectable} from '@angular/core';
import {ExtremumCalculator} from "../../../models/calculator.model";
import {FunctionOfManyArguments, FunctionOfOneArgument} from "../../../models/functions-types";
import {BehaviorSubject, Observable} from "rxjs";
import {FastestGradientDescentIteration} from "../../../models/iteration.model";
import {MultiDimensionalResult, OneDimensionalResult} from "../../../models/result-interfaces";
import * as math from "mathjs";
import {FunctionData} from "../../../models/function-data";
import {NumericVector} from "../../../models/vectors/vector.model";
import {Methods} from "../../../models/methods-enum";
import {NumericCoordinates} from "../../../models/vectors/coordinates.model";
import {Gradient} from "../../../models/vectors/gradient.model";
import {FastestGradientDescentInputValues} from "../../../models/input-values.model";
import {throwErrorIfOverflows} from "../../../utils/numbers";
import {FibonacciCalculatorService} from "../fibonacci-calculator-service/fibonacci-calculator.service";

@Injectable({
  providedIn: 'root'
})
export class FastestGradientDescentCalculatorService implements ExtremumCalculator {
  private x0!: NumericCoordinates;
  private e1!: number;
  private e2!: number;
  private M!: number;
  private funcData!: FunctionData;

  private iterationsSubject$ = new BehaviorSubject<FastestGradientDescentIteration[]>([]);

  constructor(private fibCalculator: FibonacciCalculatorService) { }

  public getResult({x0, e1, e2, M, funcData}: FastestGradientDescentInputValues): MultiDimensionalResult | Error {
    const allArgumentsSpecified = funcData.argumentsNames && funcData.argumentsNames.length > 1;
    if (allArgumentsSpecified) {
      this.x0 = x0;
      this.e1 = e1;
      this.e2 = e2;
      this.M = M;
      this.funcData = funcData;
      try {
        this.resetIterations();
        return this.calculateResult();
      } catch (e) {
        return e;
      }
    }
    return new Error('Some arguments are not specified');
  }

  public get iterations$(): Observable<FastestGradientDescentIteration[]> {
    return this.iterationsSubject$ as Observable<FastestGradientDescentIteration[]>;
  }

  public resetIterations() {
    this.iterationsSubject$.next([]);
  }

  private calculateResult(): MultiDimensionalResult {
    let x: NumericVector[] = [new NumericVector(this.x0)];
    const gradient = new Gradient(this.funcData);

    let k = 0;
    let calculatedGradient: NumericVector = gradient.calculate(x[k].coordinates);
    let norm = calculatedGradient.norm;

    while (norm >= this.e1 && k < this.M) {
      let t = this.findMinStep(calculatedGradient.coordinates, x[k].coordinates);

      x[k + 1] = x[k].minus(calculatedGradient.multiplyBy(t));

      this.pushNextIteration({
        numberOfIteration: k,
        t,
        x: x[k]?.coordinates,
        xNext: x[k + 1]?.coordinates,
        gradient: calculatedGradient?.coordinates
      });

      if (this.isEndConditionSatisfied(x[k], x[k + 1]) &&
        (k === 0 || this.isEndConditionSatisfied(x[k - 1], x[k])))  {
        return {
          extremumPoint: x[k + 1].coordinates,
          functionValue: this.f(x[k + 1])
        };
      }

      k += 1;
      calculatedGradient = gradient.calculate(x[k].coordinates);
      norm = calculatedGradient.norm;
    }

    const functionValue = this.f(x[k]);
    throwErrorIfOverflows(functionValue);
    return {
      extremumPoint: x[k].coordinates,
      functionValue: functionValue
    };
  }

  private isEndConditionSatisfied(x: NumericVector, xNext: NumericVector): boolean {
    const xVectorsDifference = xNext.minus(x).norm;
    const f = this.f(x);
    const fNext = this.f(xNext);
    const functionValuesDifference = Math.abs(fNext - f);
    return xVectorsDifference < this.e2 && functionValuesDifference < this.e2;
  }

  private findMinStep(gradient: NumericCoordinates, x: NumericCoordinates): number {
    const funcData = this.getFunctionToMinimize(gradient, x);
    const minimizationResult = this.fibCalculator.getResult(
      {func: funcData.func as FunctionOfOneArgument}
    );
    if (minimizationResult instanceof Error) {
      throw Error(minimizationResult.message);
    }
    return (minimizationResult as OneDimensionalResult).extremumPoint;
  }

  private getFunctionToMinimize(gradient: NumericCoordinates, x: NumericCoordinates): FunctionData {
    const funcFormula = this.getFunctionFromT(gradient, x);

    return {
      plainTextFormula: funcFormula,
      func: (t: number) => math.parse(funcFormula).compile().evaluate({t}),
      methodsAvailable: [Methods.FIBONACCI],
      argumentsNames: ['t']
    };
  }

  private getFunctionFromT(gradient: NumericCoordinates, x: NumericCoordinates): string {
    // phi(t) = f(x - t * gradient)
    const tArgument: string[] = [];
    if (gradient.length !== x.length) {
      throw new Error('Gradient length and argument length do not match');
    }
    for (let i = 0; i < x.length; i += 1) {
      tArgument.push(`${x[i]} - t * (${gradient[i]})`);
    }
    const extensiveFormula = this.replaceArgsWith(tArgument);
    return math.simplify(extensiveFormula).toString();
  }

  private replaceArgsWith(replacingExpressionsVector: string[]): string {
    let result = this.funcData.plainTextFormula;
    const {argumentsNames} = this.funcData;
    for (let i = 0; i < argumentsNames.length; i += 1) {
      const argumentName = argumentsNames[i];
      result = result.replaceAll(argumentName, `(${replacingExpressionsVector[i]})`);
    }
    return result;
  }

  private f(x: NumericVector) {
    const func: FunctionOfManyArguments = this.funcData.func as FunctionOfManyArguments;
    return func(...x.coordinates);
  }

  private pushNextIteration(iteration: FastestGradientDescentIteration) {
    this.iterationsSubject$.next([...this.iterationsSubject$.value, iteration]);
  }
}

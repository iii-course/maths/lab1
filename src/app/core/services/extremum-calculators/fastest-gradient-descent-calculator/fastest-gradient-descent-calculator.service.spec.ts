import {TestBed} from '@angular/core/testing';
import {FastestGradientDescentCalculatorService} from './fastest-gradient-descent-calculator.service';
import {Methods} from "../../../models/methods-enum";
import {MultiDimensionalResult} from "../../../models/result-interfaces";
import {NumericVector} from "../../../models/vectors/vector.model";
import {FastestGradientDescentInputValues} from "../../../models/input-values.model";

describe('FastestGradientDescentCalculatorService', () => {
  let service: FastestGradientDescentCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FastestGradientDescentCalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('functions of 2 arguments', () => {
    let inputValues: Partial<FastestGradientDescentInputValues>;

    beforeEach(() => {
      inputValues = {
        M: 10,
        e1: 0.01,
        e2: 0.01,
        x0: [0, 0],
      };
    });

    it('should return valid value for x^2 + x*y + y^2 - 3*x - 6*y', () => {
      inputValues.funcData = {
        plainTextFormula: 'x^2 + x*y + y^2 - 3*x - 6*y',
        func: (x: number, y: number) => {
          return Math.pow(x, 2) + x * y + Math.pow(y, 2) - 3 * x - 6 * y;
        },
        methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
        argumentsNames: ['x', 'y'],
      };

      const expectedResult: MultiDimensionalResult = {
        extremumPoint: [0, 3],
        functionValue: -9,
      };

      const result = service.getResult(inputValues as FastestGradientDescentInputValues);

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);
      expect(
        areEqual(
          result as MultiDimensionalResult,
          expectedResult,
          inputValues.e1 as number,
          inputValues.e2 as number
        )
      ).toBeTrue();
    });

    it('should return valid value for x^2 - x*y + y^2 + 3*x - 2*y + 1', () => {
      inputValues.funcData = {
        plainTextFormula: 'x^2 - x*y + y^2 + 3*x - 2*y + 1',
        func: (x: number, y: number) => {
          return Math.pow(x, 2) - x * y + Math.pow(y, 2) + 3 * x - 2 * y + 1;
        },
        methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
        argumentsNames: ['x', 'y'],
      };

      const expectedResult: MultiDimensionalResult = {
        extremumPoint: [-4 / 3, 1 / 3],
        functionValue: -4 / 3,
      };

      const result = service.getResult(inputValues as FastestGradientDescentInputValues);

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);
      expect(
        areEqual(
          result as MultiDimensionalResult,
          expectedResult,
          inputValues.e1 as number,
          inputValues.e2 as number
        )
      ).toBeTrue();
    });

    it('should return valid value for x^3 + y^3 - 3*x*y', () => {
      inputValues.x0 = [0.5, 0.5];
      inputValues.funcData = {
        plainTextFormula: 'x^3 + y^3 - 3*x*y',
        func: (x: number, y: number) => {
          return Math.pow(x, 3) + Math.pow(y, 3) - 3 * x * y;
        },
        methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
        argumentsNames: ['x', 'y'],
      };

      const expectedResult: MultiDimensionalResult = {
        extremumPoint: [1, 1],
        functionValue: -1,
      };

      const result = service.getResult(inputValues as FastestGradientDescentInputValues);

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);
      expect(
        areEqual(
          result as MultiDimensionalResult,
          expectedResult,
          inputValues.e1 as number,
          inputValues.e2 as number
        )
      ).toBeTrue();
    });
  });

  function areEqual(result: MultiDimensionalResult, expectedResult: MultiDimensionalResult,
    accuracyForPoint: number, accuracyForFunctionValue: number): boolean {
    const expectedPoint = new NumericVector(expectedResult.extremumPoint);
    const point = new NumericVector(result.extremumPoint);
    const difference = expectedPoint.minus(point);

    const expectedFuncValue = expectedResult.functionValue;
    const funcValue = result.functionValue;

    return difference.norm < accuracyForPoint &&
      Math.abs(expectedFuncValue - funcValue) < accuracyForFunctionValue;
  }
});

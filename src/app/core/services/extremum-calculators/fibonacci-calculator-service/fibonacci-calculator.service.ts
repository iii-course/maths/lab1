import {Injectable} from '@angular/core';
import {Interval} from "../../../models/interval.model";
import {OneDimensionalResult} from "../../../models/result-interfaces";
import {FunctionOfOneArgument} from "../../../models/functions-types";
import {BehaviorSubject, Observable} from "rxjs";
import {equals, throwErrorIfOverflows} from "../../../utils/numbers";
import {FibonacciIteration} from "../../../models/iteration.model";
import {FibonacciInputValues} from "../../../models/input-values.model";
import {ExtremumCalculator} from "../../../models/calculator.model";
import {SvenAlgorithmCalculatorService} from "../../sven-algorithm-calculator-service/sven-algorithm-calculator.service";

interface IntermediateValuesWithIterationIndex {
  x?: number,
  k: number,
  fy: number,
  fz: number
}

interface IntermediateValues {
  x?: number,
  fy: number,
  fz: number
}

@Injectable({
  providedIn: 'root'
})
export class FibonacciCalculatorService implements ExtremumCalculator {
  private fibonacciNumbers: number[] = [
    1, 1, 2, 3, 5, 8, 13, 21, 34,
    55, 89, 144, 233, 377, 610,
    987, 1597, 2584, 4181, 6765,
    10946, 17711, 28657, 46368,
    75025, 121393, 196418, 317811
  ];
  private funcCash: Map<number, number> = new Map<number, number>();
  private a: number[] = [];
  private b: number[] = [];
  private y: number[] = [];
  private z: number[] = [];
  private N: number = 1;

  private interval!: Interval;
  private resultIntervalLength!: number;
  private constant!: number;
  private func!: FunctionOfOneArgument;

  private iterationsSubject$ = new BehaviorSubject<FibonacciIteration[]>([]);

  constructor(private intervalCalculator: SvenAlgorithmCalculatorService) { }

  public getResult(
    { interval, resultIntervalLength, constant, func }: FibonacciInputValues
  ): OneDimensionalResult | Error {
    this.resetAll();

    this.interval = interval || this.intervalCalculator.getInterval({func: func as FunctionOfOneArgument});
    this.resultIntervalLength = resultIntervalLength || this.resultIntervalLength;
    this.constant = constant || this.constant;
    this.func = func || this.func;

    const allArgumentsProvided = this.interval && this.resultIntervalLength && this.constant && this.func;
    if (allArgumentsProvided) {
      try {
        return this.calculate();
      } catch (e) {
        return e;
      }
    }
    return new Error('Some arguments are not provided');
  }

  public get iterations$(): Observable<FibonacciIteration[]> {
    return this.iterationsSubject$ as Observable<FibonacciIteration[]>;
  }

  private calculate(): OneDimensionalResult {
    this.initializePointsArrays();

    let k: number = 0;
    let fy: number, fz: number;
    const {a, b, y, z, constant, N} = this;

    for (; k <= N - 3; k += 1) {
      fy = this.f(y[k]);
      fz = this.f(z[k]);
      this.pushNextIteration({k, fy, fz});
      this.recalculatePoints({fy, fz, k});
    }

    z[N - 2] = (a[N - 2] + b[N - 2]) / 2;
    y[N - 1] = z[N - 2];
    y[N - 2] = z[N - 2];

    z[N - 1] = y[N - 1] + constant;

    fy = this.f(y[N - 1]);
    fz = this.f(z[N - 1]);

    const resultInterval = this.calculateResultInterval({fy, fz});
    const x = (resultInterval.begin + resultInterval.end) / 2;
    this.pushNextIteration({x, k, fy, fz});

    throwErrorIfOverflows(x);
    return {
      interval: resultInterval,
      extremumPoint: x,
      functionValue: this.f(x)
    };
  }

  private f(x: number): number {
    if (!this.funcCash.get(x)) {
      this.funcCash.set(x, this.func(x));
    }
    return this.funcCash.get(x) || this.func(x);
  }

  private initializePointsArrays(): void {
    const {interval, resultIntervalLength, fibonacciNumbers} = this;

    const N = this.getIndexOfFibNumberRightAfter(Math.ceil(interval.length / resultIntervalLength));
    this.N = N;

    this.a[0] = interval.begin;
    this.b[0] = interval.end;

    this.y[0] = this.a[0] + (this.b[0] - this.a[0]) * fibonacciNumbers[N - 2] / fibonacciNumbers[N];
    this.z[0] = this.a[0] + (this.b[0] - this.a[0]) * fibonacciNumbers[N - 1] / fibonacciNumbers[N];
  }

  private recalculatePoints({fy, fz, k}: IntermediateValuesWithIterationIndex): void {
    const {a, b, y, z, fibonacciNumbers, N} = this;

    if (fy < fz || equals(fy, fz)) {
      a[k + 1] = a[k];
      b[k + 1] = z[k];
      z[k + 1] = y[k];
      y[k + 1] = a[k + 1] + (b[k + 1] - a[k + 1]) * fibonacciNumbers[N - k - 3] / fibonacciNumbers[N - k - 1];
    } else {
      a[k + 1] = y[k];
      b[k + 1] = b[k];
      y[k + 1] = z[k];
      z[k + 1] = a[k + 1] + (b[k + 1] - a[k + 1]) * fibonacciNumbers[N - k - 2] / fibonacciNumbers[N - k - 1];
    }
  }

  private calculateResultInterval({fy, fz}: IntermediateValues): Interval {
    const {a, b, y, z, N} = this;

    if (fy < fz || equals(fy, fz)) {
      a[N - 1] = a[N - 2];
      b[N - 1] = z[N - 1];
    } else {
      a[N - 1] = y[N - 1];
      b[N - 1] = b[N - 2];
    }

    return new Interval({
      begin: a[N - 1],
      end: b[N - 1]
    });
  }

  private getIndexOfFibNumberRightAfter(max: number): number {
    let currentIndex: number = this.getLastFibNumberIndex();
    let currentFibNumber: number = this.getLastFibNumber();

    if (currentFibNumber === max) {
      return currentIndex;
    } else if (currentFibNumber > max) {
      return this.fibonacciNumbers.findIndex(num => num >= max);
    } else {
      while (currentFibNumber < max) {
        currentFibNumber = this.pushNextFibNumber();
        currentIndex += 1;
      }
      return currentIndex;
    }
  }

  private pushNextFibNumber(): number {
    const prevNumber = this.fibonacciNumbers[this.getLastFibNumberIndex() - 1];
    const curNumber = this.getLastFibNumber();
    const nextNumber = curNumber + prevNumber;
    this.fibonacciNumbers.push(nextNumber);
    return nextNumber;
  }

  private getLastFibNumber(): number {
    return this.fibonacciNumbers[this.getLastFibNumberIndex()];
  }

  private getLastFibNumberIndex(): number {
    return this.fibonacciNumbers.length - 1;
  }

  private pushNextIteration({x, k, fy, fz}: IntermediateValuesWithIterationIndex): void {
    const {a, b, y, z} = this;

    this.iterationsSubject$.next([...this.iterationsSubject$.value, {
      numberOfIteration: k as number,
      interval: new Interval({begin: a[k], end: b[k]}),
      y: y[k],
      z: z[k],
      fy,
      fz,
      x
    }]);
  }

  private resetAll() {
    this.resetIterations();
    this.clearCash();
    this.resetVariables();
  }

  private resetVariables() {
    this.a = [];
    this.b = [];
    this.y = [];
    this.z = [];
    this.N = 1;
    this.interval = new Interval({begin: 0, end: 10});
    this.resultIntervalLength = 0.1;
    this.constant = 0.01;
  }

  private resetIterations() {
    this.iterationsSubject$.next([]);
  }

  private clearCash() {
    this.funcCash.clear();
  }
}

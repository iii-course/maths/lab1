import { TestBed } from '@angular/core/testing';

import { FibonacciCalculatorService } from './fibonacci-calculator.service';
import {Interval} from "../../../models/interval.model";
import {OneDimensionalResult} from "../../../models/result-interfaces";
import {equals} from "../../../utils/numbers";
import {SvenAlgorithmCalculatorService} from "../../sven-algorithm-calculator-service/sven-algorithm-calculator.service";

describe('FibonacciCalculatorService', () => {
  let service: FibonacciCalculatorService;

  beforeEach(() => {
    service = new FibonacciCalculatorService(new SvenAlgorithmCalculatorService());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when interval is not specified', () => {
    it('should return valid value for x^2', () => {
      const func = (x: number) => Math.pow(x, 2);

      const result = service.getResult({func});

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 0;

      expect(equals(pointFound, expectedPoint, (service as any).resultIntervalLength)).toBeTrue();
    });

    it('should return valid value for (x - 5)^2', () => {
      const func = (x: number) => Math.pow((x - 5), 2);

      const result = service.getResult({func});

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 5;
      expect(equals(pointFound, expectedPoint, (service as any).resultIntervalLength)).toBeTrue();
    });

    it('should return valid value for 2 * x^2 - 12 * x', () => {
      const func = (x: number) => {
        return 2 * Math.pow(x, 2) - 12 * x;
      };

      const result = service.getResult({func});

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 3;

      expect(equals(pointFound, expectedPoint, (service as any).resultIntervalLength)).toBeTrue();
    });
  });

  describe('when interval is specified', () => {
    it('should return valid value for x^2', () => {
      const func = (x: number) => Math.pow(x, 2);
      const resultIntervalLength = 0.01;

      const result = service.getResult({
        interval: new Interval({begin: -10, end: 10}),
        resultIntervalLength,
        constant: 0.001,
        func
      });

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 0;

      expect(equals(pointFound, expectedPoint, resultIntervalLength)).toBeTrue();
    });

    it('should return valid value for x^3 - 3 * x^2 - 4', () => {
      const func = (x: number) => {
        return Math.pow(x, 3) - 3 * Math.pow(x, 2) - 4;
      };
      const resultIntervalLength = 0.01;

      const result = service.getResult({
        interval: new Interval({begin: 0, end: 10}),
        resultIntervalLength,
        constant: 0.001,
        func
      });

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 2;

      expect(equals(pointFound, expectedPoint, resultIntervalLength)).toBeTrue();
    });

    it('should return valid value for 2 * x^2 - 12 * x', () => {
      const func = (x: number) => {
        return 2 * Math.pow(x, 2) - 12 * x;
      };
      const resultIntervalLength = 1;

      const result = service.getResult({
        interval: new Interval({begin: 0, end: 10}),
        resultIntervalLength,
        constant: 0.01,
        func
      });

      expect(result).toBeDefined();
      expect(result).not.toBeInstanceOf(Error);

      const pointFound = (result as OneDimensionalResult).extremumPoint;
      const expectedPoint = 2.697;

      expect(equals(pointFound, expectedPoint, resultIntervalLength)).toBeTrue();
    });
  });
});

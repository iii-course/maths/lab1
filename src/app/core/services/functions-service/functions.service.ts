import { Injectable } from '@angular/core';
import {FunctionData} from "../../models/function-data";
import {Methods} from "../../models/methods-enum";

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {
  private methodsForFunctionsOfOneArgument = [
    Methods.FIBONACCI
  ];

  private methodsForFunctionsOfManyArguments = [
    Methods.FASTEST_GRADIENT_DESCENT
  ];

  private functions: FunctionData[] = [
    {
      plainTextFormula: 'x^2 + 4*e^(-x/4)',
      func: (x: number) => {
        return Math.pow(x, 2) +
          4 * Math.pow(Math.E, -x / 4);
      },
      methodsAvailable: this.methodsForFunctionsOfOneArgument,
      argumentsNames: ['x'],
    },
    {
      plainTextFormula: 'x^2 + 7*z^2 - 4*x*z - 3*y - 2*z',
      func: (x: number, y: number, z: number) => {
        return Math.pow(x, 2) +
          7 * Math.pow(z, 2) -
          4 * x * z -
          3 * y -
          2 * z;
      },
      methodsAvailable: this.methodsForFunctionsOfManyArguments,
      argumentsNames: ['x', 'y', 'z'],
    },
    {
      plainTextFormula: '2*x^2 + x*y + y^2',
      func: (x: number, y: number) => {
        return 2 * Math.pow(x, 2) + x * y + Math.pow(y, 2);
      },
      methodsAvailable: this.methodsForFunctionsOfManyArguments,
      argumentsNames: ['x', 'y'],
    },
    {
      plainTextFormula: 'x^2 + x*y + y^2 - 3*x - 6*y',
      func: (x: number, y: number) => {
        return Math.pow(x, 2) + x * y + Math.pow(y, 2) - 3 * x - 6 * y;
      },
      methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
      argumentsNames: ['x', 'y'],
    },
    {
      plainTextFormula: 'x^2 - x*y + y^2 + 3*x - 2*y + 1',
      func: (x: number, y: number) => {
        return Math.pow(x, 2) - x * y + Math.pow(y, 2) + 3 * x - 2 * y + 1;
      },
      methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
      argumentsNames: ['x', 'y'],
    },
    {
      plainTextFormula: 'x^3 + y^3 - 3*x*y',
      func: (x: number, y: number) => {
        return Math.pow(x, 3) + Math.pow(y, 3) - 3 * x * y;
      },
      methodsAvailable: [Methods.FASTEST_GRADIENT_DESCENT],
      argumentsNames: ['x', 'y'],
    }
  ];

  constructor() {}

  get allFunctions() {
    return [...this.functions];
  }
}

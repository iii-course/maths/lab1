export function equals(f1: number, f2: number, tolerance: number = 0.00001) {
  return Math.abs(f1 - f2) < tolerance;
}

export function throwErrorIfOverflows(n: number) {
  if (!Number.isSafeInteger(Math.trunc(n))) {
    throw new Error(`Sorry, the number is too large: ${n}. Consider choosing another interval`);
  }
}

export function generateRandomInteger(min: number = -1000, max: number = 1000) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

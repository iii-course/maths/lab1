import { Pipe, PipeTransform } from '@angular/core';
import {NumericCoordinates} from "../../models/vectors/coordinates.model";

@Pipe({
  name: 'coordinatesArray'
})
export class CoordinatesArrayPipe implements PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform {

  transform(value: NumericCoordinates | number, ...args: unknown[]): string {
    if (Array.isArray(value)) {
      return value && value.length > 0 ? `[${value.join(', ')}]` : '[]';
    }
    return `[${value}]`;
  }

}

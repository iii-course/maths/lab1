import { Pipe, PipeTransform } from '@angular/core';

function toFixed(value: number, precision: number): number {
  const power = Math.pow(10, precision || 0);
  return Math.round(value * power) / power;
}

@Pipe({
  name: 'numberFormatting'
})
export class NumberFormattingPipe implements PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform, PipeTransform {

  transform(value: number | number[], ...args: unknown[]): number | number[] {
    if (!value) {
      return value;
    }
    const NUM_OF_DIGITS_AFTER_POINT = 4;
    if (Array.isArray(value)) {
      return value.map(num => toFixed(num, NUM_OF_DIGITS_AFTER_POINT)) as number[];
    }
    return toFixed(value, NUM_OF_DIGITS_AFTER_POINT) as number;
  }

}

import { FastestGradientDescentCalculatorComponent } from './fastest-gradient-descent-calculator.component';
import {FormBuilder} from "@angular/forms";
import { MockService } from 'ng-mocks';
import {FastestGradientDescentCalculatorService} from "../../../core/services/extremum-calculators/fastest-gradient-descent-calculator/fastest-gradient-descent-calculator.service";

describe('FastestGradientDescentCalculatorComponent', () => {
  let component: FastestGradientDescentCalculatorComponent;

  beforeEach(() => {
    component = new FastestGradientDescentCalculatorComponent(
      MockService(FormBuilder),
      MockService(FastestGradientDescentCalculatorService)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

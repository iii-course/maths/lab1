import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FastestGradientDescentCalculatorService}
  from "../../../core/services/extremum-calculators/fastest-gradient-descent-calculator/fastest-gradient-descent-calculator.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MultiDimensionalResult} from "../../../core/models/result-interfaces";
import {FunctionData} from "../../../core/models/function-data";


@Component({
  selector: 'app-fastest-gradient-descent-calculator',
  templateUrl: './fastest-gradient-descent-calculator.component.html',
  styleUrls: ['./fastest-gradient-descent-calculator.component.scss']
})
export class FastestGradientDescentCalculatorComponent implements OnInit, OnChanges {
  @Input() funcData!: FunctionData;

  constructor(
    private fb: FormBuilder,
    private calculator: FastestGradientDescentCalculatorService
  ) { }

  inputValuesFormGroup!: FormGroup;
  result: MultiDimensionalResult | undefined;
  errorMessage: string | undefined;
  displayedColumns: string[] = ['numberOfIteration', 'gradient', 't', 'x', 'xNext'];

  ngOnInit(): void {
    this.inputValuesFormGroup = this.fb.group({
      x0: this.fb.array([]),
      e1: [0.1, [Validators.required, Validators.min(0.000001)]],
      e2: [0.15, [Validators.required, Validators.min(0.000001)]],
      M: [10, [Validators.required, Validators.min(1)]],
    });
    this.refillFormCoordinatesArray();
  }

  ngOnChanges() {
    if (this.inputValuesFormGroup) {
      this.refillFormCoordinatesArray();
    }
    this.result = undefined;
    this.calculator.resetIterations();
  }

  public get coordinatesInput(): FormArray {
    return this.inputValuesFormGroup?.get('x0') as FormArray;
  }

  public get iterations$() {
    return this.calculator.iterations$;
  }

  public onSubmit() {
    this.resetAll();
    this.setResult();
  }

  private setResult() {
    const {x0, e1, e2, M} = this.inputValuesFormGroup.value;
    const result = this.calculator.getResult({x0, e1, e2, M, funcData: this.funcData});
    if (result instanceof Error) {
      this.errorMessage = result.message;
    } else {
      this.result = result;
    }
  }

  private resetAll() {
    this.clearErrorMessage();
    this.clearResult();
  }

  private refillFormCoordinatesArray(): void {
    this.coordinatesInput.clear();
    for (let i = 0; i < this.funcData.argumentsNames.length; i += 1) {
      this.addCoordinateInput();
    }
  }

  private addCoordinateInput(): void {
    this.coordinatesInput.push(this.fb.control(0, Validators.required));
  }

  private clearErrorMessage() {
    this.errorMessage = undefined;
  }

  private clearResult() {
    this.result = undefined;
  }
}

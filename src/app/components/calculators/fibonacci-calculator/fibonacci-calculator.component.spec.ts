import { FibonacciCalculatorComponent } from './fibonacci-calculator.component';
import {FormBuilder} from "@angular/forms";
import { MockService } from 'ng-mocks';
import {FibonacciCalculatorService} from "../../../core/services/extremum-calculators/fibonacci-calculator-service/fibonacci-calculator.service";

describe('FibonacciCalculatorComponent', () => {
  let component: FibonacciCalculatorComponent;

  beforeEach(() => {
    component = new FibonacciCalculatorComponent(
      MockService(FormBuilder),
      MockService(FibonacciCalculatorService)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';
import {Interval} from "../../../core/models/interval.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { FibonacciCalculatorService} from
  "../../../core/services/extremum-calculators/fibonacci-calculator-service/fibonacci-calculator.service";
import {OneDimensionalResult} from "../../../core/models/result-interfaces";
import {FunctionOfOneArgument} from "../../../core/models/functions-types";


@Component({
  selector: 'app-fibonacci-calculator',
  templateUrl: './fibonacci-calculator.component.html',
  styleUrls: ['./fibonacci-calculator.component.scss']
})
export class FibonacciCalculatorComponent implements OnInit {
  private interval!: Interval;

  @Input() func!: FunctionOfOneArgument;
  result: OneDimensionalResult | undefined;
  errorMessage: string | undefined;
  displayedColumns = ['numberOfIteration', 'a', 'y', 'z', 'b', 'fy', 'fz', 'x'];
  inputValuesFormGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private calculator: FibonacciCalculatorService
  ) { }

  public ngOnInit(): void {
    this.inputValuesFormGroup = this.fb.group({
      resultIntervalLength: [null, [Validators.required, Validators.min(0.000001)]],
      constant: [null, [Validators.required, Validators.min(0.000001)]]
    });
  }

  public get iterations$() {
    return this.calculator.iterations$;
  }

  public setInterval(i: Interval) {
    this.interval = i;
    this.onSubmit();
  }

  public onSubmit() {
    this.resetAll();
    this.setResult();
  }

  private setResult() {
    const {resultIntervalLength, constant} = this.inputValuesFormGroup.value;
    const result = this.calculator.getResult({
      interval: this.interval,
      func: this.func,
      resultIntervalLength,
      constant
    });
    if (result instanceof Error) {
      this.errorMessage = result.message;
    } else {
      this.result = result;
    }
  }

  private resetAll() {
    this.clearErrorMessage();
    this.clearResult();
  }

  private clearErrorMessage() {
    this.errorMessage = undefined;
  }

  private clearResult() {
    this.result = undefined;
  }
}

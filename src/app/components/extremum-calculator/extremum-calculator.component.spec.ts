import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtremumCalculatorComponent } from './extremum-calculator.component';

describe('ExtremumCalculatorComponent', () => {
  let component: ExtremumCalculatorComponent;
  let fixture: ComponentFixture<ExtremumCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtremumCalculatorComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtremumCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

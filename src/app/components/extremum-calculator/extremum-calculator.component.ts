import {Component, Input} from '@angular/core';
import {FunctionData} from "../../core/models/function-data";
import {Methods} from "../../core/models/methods-enum";
import {FunctionOfManyArguments, FunctionOfOneArgument} from "../../core/models/functions-types";

@Component({
  selector: 'app-extremum-calculator',
  templateUrl: './extremum-calculator.component.html',
  styleUrls: ['./extremum-calculator.component.scss']
})
export class ExtremumCalculatorComponent {
  @Input() funcData!: FunctionData | null;
  @Input() method!: Methods | null;

  methods = Methods;

  constructor() {}

  get funcOfOneArgument(): FunctionOfOneArgument {
    return this.funcData?.func as FunctionOfOneArgument;
  }

  get funcOfManyArguments(): FunctionOfManyArguments {
    return this.funcData?.func as FunctionOfManyArguments;
  }
}

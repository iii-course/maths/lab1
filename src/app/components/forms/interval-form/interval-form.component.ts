import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Interval} from "../../../core/models/interval.model";
import {BehaviorSubject, Subject, Subscription} from "rxjs";

function intervalValidator(beginNumber: number) {
  return (control: AbstractControl): ValidationErrors | null => {
    const endNumber = control.value;
    return endNumber <= beginNumber ? { message: 'Invalid interval' } : null;
  };

}

@Component({
  selector: 'app-interval-form',
  templateUrl: './interval-form.component.html',
  styleUrls: ['./interval-form.component.scss']
})
export class IntervalFormComponent implements OnInit, OnDestroy {
  @Output() intervalChange = new EventEmitter<Interval>();
  intervalFormGroup!: FormGroup;
  beginLabel: string = '-∞';
  endLabel: string = '∞';

  subscription = new Subscription();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.intervalFormGroup = this.fb.group({
      begin: [Number.NEGATIVE_INFINITY, Validators.required],
      end: [Infinity, Validators.required]
    });
    this.onChanges();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public get isFormValid() {
    const {begin, end} = this.intervalFormGroup.value;
    return this.intervalFormGroup.valid && end > begin;
  }

  public onChanges(): void {
    this.subscription.add(
      this.intervalFormGroup.valueChanges.subscribe(() => this.updateLabels())
    );
  }

  public onSubmit() {
    this.intervalChange.emit(new Interval(this.intervalFormGroup.value));
  }

  public onReset() {
    this.intervalFormGroup.setValue({
      begin: Number.NEGATIVE_INFINITY,
      end: Infinity
    }, {emitEvent: true});
  }

  private updateLabels() {
    const {begin, end} = this.intervalFormGroup.value;
    this.beginLabel = begin === Number.NEGATIVE_INFINITY ? '-∞' : 'Begin';
    this.endLabel = end === Infinity ? '∞' : 'End';
  }
}

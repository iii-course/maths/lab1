import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalFormComponent } from './interval-form.component';
import {MockService} from "ng-mocks";
import {FormBuilder} from "@angular/forms";

describe('IntervalFormComponent', () => {
  let component: IntervalFormComponent;

  beforeEach(() => {
    component = new IntervalFormComponent(
      MockService(FormBuilder),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

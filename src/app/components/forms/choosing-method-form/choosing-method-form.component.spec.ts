import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosingMethodFormComponent } from './choosing-method-form.component';
import {MockService} from "ng-mocks";
import {FormBuilder} from "@angular/forms";

describe('ChoosingMethodFormComponent', () => {
  let component: ChoosingMethodFormComponent;

  beforeEach(() => {
    component = new ChoosingMethodFormComponent(
      MockService(FormBuilder)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FunctionData} from "../../../core/models/function-data";
import {Methods} from "../../../core/models/methods-enum";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-choosing-method-form',
  templateUrl: './choosing-method-form.component.html',
  styleUrls: ['./choosing-method-form.component.scss']
})
export class ChoosingMethodFormComponent implements OnInit {
  @Input() disabled!: boolean;
  @Input() methods!: Methods[];
  @Output() methodChange = new EventEmitter<Methods>();
  methodsFormGroup!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.methodsFormGroup = this.fb.group({
      method: [null, [Validators.required]]
    });
  }

  public onSubmit() {
    const chosenMethod = this.methodsFormGroup.value.method;
    this.methodChange.emit(chosenMethod);
  }
}

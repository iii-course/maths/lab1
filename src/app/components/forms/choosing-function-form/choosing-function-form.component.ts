import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FunctionData} from "../../../core/models/function-data";
import {FunctionsService} from "../../../core/services/functions-service/functions.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-choosing-function-form',
  templateUrl: './choosing-function-form.component.html',
  styleUrls: ['./choosing-function-form.component.scss']
})
export class ChoosingFunctionFormComponent implements OnInit {
  @Input() disabled!: boolean;
  @Output() functionChange = new EventEmitter<FunctionData>();
  functions!: FunctionData[];
  functionFormGroup!: FormGroup;

  constructor(private functionsService: FunctionsService,
    private fb: FormBuilder) { }

  public ngOnInit() {
    this.functions = this.functionsService.allFunctions;
    this.functionFormGroup = this.fb.group({
      functionData: [null, [Validators.required]],
    });
  }

  get functionData() {
    return this.functionFormGroup.get('functionData') as FormControl;
  }

  public onSubmit() {
    const chosenFunc = this.functionFormGroup.value.functionData;
    this.functionChange.emit(chosenFunc);
  }
}

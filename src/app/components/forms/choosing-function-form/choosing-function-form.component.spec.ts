import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosingFunctionFormComponent } from './choosing-function-form.component';
import {Mock, MockService} from "ng-mocks";
import {FormBuilder} from "@angular/forms";
import {FunctionsService} from "../../../core/services/functions-service/functions.service";

describe('ChoosingFunctionFormComponent', () => {
  let component: ChoosingFunctionFormComponent;

  beforeEach(() => {
    component = new ChoosingFunctionFormComponent(
      MockService(FunctionsService),
      MockService(FormBuilder),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

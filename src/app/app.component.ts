import { Component } from '@angular/core';
import {FunctionData} from "./core/models/function-data";
import {Subject} from "rxjs";
import {Methods} from "./core/models/methods-enum";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentFunction$ = new Subject<FunctionData>();
  currentMethod$ = new Subject<Methods>();
  currentFunction!: FunctionData;
  currentMethod!: Methods;

  areFormsDisabled: boolean = false;

  public onFunctionChange(functionData: FunctionData) {
    this.currentFunction$.next(functionData);
    this.currentFunction = functionData;
    this.areFormsDisabled = true;
  }

  public onMethodChange(method: Methods) {
    this.currentMethod$.next(method);
    this.currentMethod = method;
    this.areFormsDisabled = true;
  }

  public onReset() {
    this.areFormsDisabled = false;
  }
}

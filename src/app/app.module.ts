import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChoosingFunctionFormComponent } from './components/forms/choosing-function-form/choosing-function-form.component';
import { ChoosingMethodFormComponent } from './components/forms/choosing-method-form/choosing-method-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ExtremumCalculatorComponent } from './components/extremum-calculator/extremum-calculator.component';
import { FibonacciCalculatorComponent } from './components/calculators/fibonacci-calculator/fibonacci-calculator.component';
import { IntervalFormComponent } from './components/forms/interval-form/interval-form.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from '@angular/material/table';
import { FastestGradientDescentCalculatorComponent } from './components/calculators/fastest-gradient-descent-calculator/fastest-gradient-descent-calculator.component';
import { CoordinatesArrayPipe } from './core/pipes/coordinates-array-pipe/coordinates-array.pipe';
import { NumberFormattingPipe } from './core/pipes/number-formatting-pipe/number-formatting.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ChoosingFunctionFormComponent,
    ChoosingMethodFormComponent,
    ExtremumCalculatorComponent,
    FibonacciCalculatorComponent,
    IntervalFormComponent,
    FastestGradientDescentCalculatorComponent,
    CoordinatesArrayPipe,
    NumberFormattingPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
